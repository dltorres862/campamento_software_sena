<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use File;
use App\Models\Bootcamps;


class BootcampsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //conectarnos al archivo json 
        $json = File::get("database/_data/bootcamps.json");
        $arrays_bootcamps=json_decode($json);
        //recorrrer el archivo 
        foreach($arrays_bootcamps as $b){
             //por cada instancia crear un bootcamp
             $bootcamp = new Bootcamps();
             $bootcamp->name=$b->name;
             $bootcamp->description = $b->description;
             $bootcamp->website = $b->website;
             $bootcamp->phone = $b->phone;
             $bootcamp->user_id = $b->user_id;
             $bootcamp->average_rating = $b->average_rating;
             $bootcamp->average_cost = $b->average_cost;
             $bootcamp->save();



        }
        
    }
}
