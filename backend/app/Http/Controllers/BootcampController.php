<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bootcamps;

class BootcampController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Bootcamps::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    //1.traer el playload
    $datos=$request->all();
    //crear el nuevo bootcamp
    Bootcamps::create($datos);
    return "bootcamp creado";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo"mostrar un nuevo bootcamps espesifico cuyo id es $id ";
        return Bootcamps::Find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //1. localizar el bootcamps con id
        $b = Bootcamps::find($id);
        //2. actualizat con update
        $b->update($request->all());
        return"bootcamps actualizado";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //1. localizar el bootcamps con id
        $b = Bootcamps::find($id);
        //2. actualizat con update
        $b->delete();
        return"bootcamps eliminado";
    }
}
